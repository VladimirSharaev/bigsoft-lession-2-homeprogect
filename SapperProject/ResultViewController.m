//
//  ResultViewController.m
//  SapperProject
//
//  Created by Vladimir on 14.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "ResultViewController.h"

@interface ResultViewController ()

@end

@implementation ResultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.nameLabel.text = self.player.playerName;
    if (self.player.timeInterval != nil) {
        self.timeLabel.text = self.player.timeInterval;
    }else{
        self.timeLabel.text = @"Player loses"; 
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
