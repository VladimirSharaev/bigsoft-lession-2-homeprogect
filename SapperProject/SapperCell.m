//
//  SapperCell.m
//  SapperProject
//
//  Created by Vladimir on 03.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "SapperCell.h"

@implementation SapperCell

#pragma mark - Methods

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.haveCellMine = NO;
        self.cellType = CellTypeCloseCell;
    }
    return self;
}

@end
