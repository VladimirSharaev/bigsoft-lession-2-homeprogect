//
//  ViewController.h
//  SapperProject
//
//  Created by Vladimir on 03.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SapperPlayer.h"
#import "SapperField.h"
#import "SapperCell.h"
#import "ResultViewController.h"

typedef NS_ENUM(NSUInteger, GameStateType){
    GameStateTypeBeforeGame,
    GameStateTypeInGame,
    GameStateTypeInPause,
    GameStateTypeIsOverGame,
    GameStateTypeCount
};

@interface ViewController : UIViewController

#pragma mark - Propertes

@property (nonatomic) SapperPlayer *player;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) GameStateType gameState;
@property (nonatomic) NSDate *dateInStartGame;
@property (nonatomic) NSMutableArray *arrayOfCells;
@property (nonatomic) SapperField *gameField;
@property (nonatomic) NSMutableArray *openCells;

#pragma mark - ViewPropertes

@property (weak, nonatomic) IBOutlet UITextField *nameTextBox;
@property (weak, nonatomic) IBOutlet UIButton *startGameButton;
@property (weak, nonatomic) IBOutlet UIButton *pauseGameButton;
@property (weak, nonatomic) IBOutlet UIButton *continueGameButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

#pragma mark - ButtonAction

- (IBAction)pressStartGameButton:(id)sender;
- (IBAction)pressPauseGameButton:(id)sender;
- (IBAction)pressContinueGameButton:(id)sender;


#pragma mark - Methods

- (BOOL)sendPlayerNameToModel:(NSString*)playerName;
- (void)timerFunction;
- (void)createSapperField;
- (void)cellButtonWasPressed:(UIButton *)pressedButton;
- (void)cellButtonWasPressedRepeat:(UIButton *)pressedButton;
- (BOOL)canOpenSapperCell:(NSInteger)index;
- (void)openSapperCell:(NSInteger)index;
- (NSMutableArray *)getIndexNearOpenCell:(NSInteger)index;
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
- (void)newGame;
- (BOOL)correctIndex:(NSInteger)index indexFromArray:(NSInteger)indexFromArray;

#pragma mark - Segues

- (void)showResult;



@end

