//
//  ViewController.m
//  SapperProject
//
//  Created by Vladimir on 03.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - StandartMethod

- (void)viewDidLoad
{
    self.gameState = GameStateTypeBeforeGame;
    
    self.gameField = [SapperField new];
    [self.gameField createField];
    self.openCells = [NSMutableArray new];
    // Do any additional setup after loading the view, typically from a nib.
}


#pragma mark - ButtonAction

- (IBAction)pressStartGameButton:(id)sender
{
    if ([self sendPlayerNameToModel:self.nameTextBox.text]) {
        self.pauseGameButton.hidden = YES;
        self.timeLabel.hidden = NO;
        self.nameTextBox.enabled = NO;
        self.startGameButton.enabled = NO;
        self.gameState = GameStateTypeInGame;
        self.dateInStartGame = [NSDate date];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01f target:self selector:@selector(timerFunction) userInfo:nil repeats:YES];
        [self createSapperField];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Enter your name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)pressPauseGameButton:(id)sender
{
    self.gameState = GameStateTypeInPause;
}

- (IBAction)pressContinueGameButton:(id)sender
{
    self.gameState = GameStateTypeInGame;
}


#pragma mark - Methods

- (void)createSapperField
{
    self.arrayOfCells = [[[NSMutableArray alloc] init] mutableCopy];
    for (NSInteger index = 0; index < FIELD_SIZE; index++) {
        UIButton *cellButton = [UIButton buttonWithType:UIButtonTypeSystem];
        CGRect frame = CGRectZero;
        frame.size.width = CELL_WIDHT;
        frame.size.height = CELL_HEIGHT;
        frame.origin.x = (index % FIELD_WIDHT_COUNT) * CELL_WIDHT + FIELD_X;
        frame.origin.y = (index / FIELD_HEIGHT_COUNT) * CELL_HEIGHT + FIELD_Y;
        cellButton.frame = frame;
        cellButton.center = CGPointMake(frame.origin.x + frame.size.width / 2.0f, frame.origin.y + frame.size.height / 2.0f);
        cellButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [cellButton setBackgroundColor:[UIColor lightGrayColor]];
        [cellButton setTintColor:[UIColor blackColor]];
        [cellButton setTitle:@"*" forState:UIControlStateNormal];
        [cellButton addTarget:self action:@selector(cellButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cellButton addTarget:self action:@selector(cellButtonWasPressedRepeat:) forControlEvents:UIControlEventTouchUpOutside];
        [self.arrayOfCells addObject:cellButton];
        [self.view addSubview:cellButton];
    }
}

- (void)cellButtonWasPressed:(UIButton *)pressedButton
{
    NSInteger index = [self.arrayOfCells indexOfObject:pressedButton];
    //NSLog(@"%ld", (long)index);
    [self openSapperCell:index];
}

- (void)cellButtonWasPressedRepeat:(UIButton *)pressedButton
{
    if ([pressedButton backgroundColor] == [UIColor redColor]) {
        [pressedButton setBackgroundColor:[UIColor lightGrayColor]];
    }
    else{
        [pressedButton setBackgroundColor:[UIColor redColor]];
    }
}

- (void)openSapperCell:(NSInteger)index
{
    UIButton *button = [self.arrayOfCells objectAtIndex:index];
    if ([self canOpenSapperCell:index]) {
        if (![self.openCells containsObject:@(index)]) {
            [self.openCells addObject:@(index)];
            NSInteger numberOfMinesInCell = [self.gameField getNumberOfMines:index];
            NSString *stringNumberOfMines = [NSString stringWithFormat:@"%ld", (long)numberOfMinesInCell];
            [button setTitle:stringNumberOfMines forState:UIControlStateNormal];
            button.enabled = NO;
            if (numberOfMinesInCell == 0)  {
                NSMutableArray *arrayOfIndex = [NSMutableArray new];
                arrayOfIndex = [self getIndexNearOpenCell:index];
                for (id object in arrayOfIndex) {
                    if ([self correctIndex:index indexFromArray:[object integerValue]]) {
                        [self openSapperCell:[object integerValue]];
                    }
                }
            }
        }
    }else{
        [button setTitle:@"M" forState:UIControlStateNormal];
        button.enabled = NO;
        if ([self.timer isValid]) {
        [self.timer invalidate];
            }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ohhh!" message:@"You lose the game!" delegate:self cancelButtonTitle:@"New game" otherButtonTitles:@"Result", @"Exit", nil];
        self.player.timeInterval = nil;
        [alert show];
    }
    
}

- (BOOL)correctIndex:(NSInteger)index indexFromArray:(NSInteger)indexFromArray
{
    NSInteger row = index / FIELD_HEIGHT_COUNT;
    NSInteger collum = index % FIELD_WIDHT_COUNT;
    NSInteger rowIndexFromArray = indexFromArray / FIELD_HEIGHT_COUNT;
    NSInteger collumIndexFromArray = indexFromArray % FIELD_WIDHT_COUNT;

    if ((rowIndexFromArray >= row - 1) && (rowIndexFromArray <= row + 1) && (collumIndexFromArray >= collum - 1) && (collumIndexFromArray <= collum + 1) && (indexFromArray >= 0) && (indexFromArray <= FIELD_SIZE - 1)) {
        return YES;
    }
    return NO;
}

- (NSMutableArray *)getIndexNearOpenCell:(NSInteger)index
{
    NSMutableArray *arrayOfIndex = [NSMutableArray new];
    [arrayOfIndex addObject:@(index + 1)];
    [arrayOfIndex addObject:@(index - 1)];
    [arrayOfIndex addObject:@(index + FIELD_HEIGHT_COUNT)];
    [arrayOfIndex addObject:@(index - FIELD_HEIGHT_COUNT)];
    [arrayOfIndex addObject:@(index + (FIELD_HEIGHT_COUNT - 1))];
    [arrayOfIndex addObject:@(index + (FIELD_HEIGHT_COUNT + 1))];
    [arrayOfIndex addObject:@(index - (FIELD_HEIGHT_COUNT - 1))];
    [arrayOfIndex addObject:@(index - (FIELD_HEIGHT_COUNT + 1))];
    return arrayOfIndex;
}

- (BOOL)canOpenSapperCell:(NSInteger)index
{
    
    if ([self.gameField haveCellMine:index]) {
        return  NO;
    }else{
        return  YES;
    }
}



- (BOOL)sendPlayerNameToModel:(NSString*)playerName
{
    self.player = [SapperPlayer new];
    if (playerName.length != 0) {
        self.player.playerName = playerName;
        return YES;
    }
    return NO;
}

-(void)timerFunction
{
    NSDate *dateNow = [NSDate date];
    NSTimeInterval timeInterval = [dateNow timeIntervalSinceDate:self.dateInStartGame];
    self.timeLabel.text = [NSString stringWithFormat:@"%1.0f",timeInterval];
    if (self.openCells.count == (FIELD_SIZE - MINE_COUNT)) {
        if ([self.timer isValid]) {
            [self.timer invalidate];
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations!" message:@"You win the game!" delegate:self cancelButtonTitle:@"New game" otherButtonTitles:@"Result", @"Exit", nil];
        self.player.timeInterval = [NSString stringWithFormat:@"%1.0f",timeInterval];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self newGame];
            break;
        case 1:
            [self newGame];
            [self showResult];
            break;
            case 2:
            exit(0);
            break;
        default:
            break;
    }
}

- (void)newGame
{
    for (id object in self.arrayOfCells) {
        UIButton *button = (UIButton *)object;
        button.enabled = NO;
    }
    [self.gameField createField];
    [self.arrayOfCells removeAllObjects];
    [self.openCells removeAllObjects];
    self.startGameButton.enabled = YES;
    self.timeLabel.hidden = YES;
    self.nameTextBox.enabled = YES;
    
}

#pragma mark - Segues

- (void)showResult
{
    BOOL useSegue = NO;
    if (useSegue) {
        [self performSegueWithIdentifier:@"ShowResult" sender:nil];
    } else {
        ResultViewController *resultViewController = [ResultViewController new];
        UIStoryboard *storyboard = self.storyboard;
        resultViewController = [storyboard instantiateViewControllerWithIdentifier:@"ResultViewController"];
        resultViewController.player = self.player;
        [self.navigationController pushViewController:resultViewController animated:YES];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowResult"]) {
        ResultViewController *resultViewController = (ResultViewController *)segue.destinationViewController;
        resultViewController.player = self.player;
    }
}

@end
