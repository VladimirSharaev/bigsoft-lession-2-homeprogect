//
//  SapperCellView.h
//  SapperProject
//
//  Created by Vladimir on 04.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SapperCellView : UIView

#pragma mark - Propertes

@property NSInteger indexInMass;
@property UIButton *cellButton;


@end
