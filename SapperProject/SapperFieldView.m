//
//  SapperFieldView.m
//  SapperProject
//
//  Created by Vladimir on 04.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "SapperFieldView.h"

@implementation SapperFieldView

#pragma mark - Methods

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self createField];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createField];
    }
    return self;
}

- (void)createField
{
    
    if (self.arrayCells.count) {
        for (SapperCellView *cell in self.arrayCells) {
            [cell removeFromSuperview];
        }
    }
    
    
    NSMutableArray *cells = [NSMutableArray new];
    for (NSInteger index = 0; index < FIELD_SIZE; index++) {
        UIButton *cellButton = [UIButton buttonWithType:UIButtonTypeSystem];
        CGRect frame = CGRectZero;
        frame.size.width = CELL_WIDHT;
        frame.size.height = CELL_HEIGHT;
        frame.origin.x = (index / FIELD_WIDHT_COUNT) * CELL_WIDHT;
        frame.origin.y = (index % FIELD_HEIGHT_COUNT) * CELL_HEIGHT;
        SapperCellView *cell = [[SapperCellView alloc] initWithFrame:frame];
        cell.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [cells addObject:cell];
        [self addSubview:cell];
    }
    self.arrayCells = cells;
}

@end
