//
//  SapperPlayer.h
//  SapperProject
//
//  Created by Vladimir on 03.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SapperPlayer : NSObject

#pragma mark - Propertes

@property (nonatomic) NSString *playerName;
@property (nonatomic) NSString *timeInterval;

@end
