//
//  SapperCell.h
//  SapperProject
//
//  Created by Vladimir on 03.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CellType){
    CellTypeOpenCell,
    CellTypeCloseCell,
    CellTypeCount
};


@interface SapperCell : NSObject

#pragma mark - Propertes

@property (nonatomic) CellType cellType;
@property (nonatomic) NSInteger numberOfMinesWhoNear;
@property (nonatomic) BOOL haveCellMine;

@end
