//
//  SapperField.h
//  SapperProject
//
//  Created by Vladimir on 03.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "SapperCell.h"

@interface SapperField : NSObject

#pragma mark - Propertes

@property (nonatomic) NSMutableArray *arrayOfCells;
@property (nonatomic) NSInteger countOfCell;
@property (nonatomic) NSInteger fieldWidht;
@property (nonatomic) NSInteger fieldHeight;

#pragma mark - Methods

- (void)createField;
- (NSInteger)getNumberOfMines:(NSInteger)index;
- (BOOL)haveCellMine:(NSInteger)index;
- (BOOL)correctIndex:(NSInteger)index indexFromArray:(NSInteger)indexFromArray;
- (NSMutableArray *)getIndexNearOpenCellSmallWersion:(NSInteger)index;

@end
