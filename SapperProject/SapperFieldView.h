//
//  SapperFieldView.h
//  SapperProject
//
//  Created by Vladimir on 04.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SapperCellView.h"
#import "Constants.h"

@interface SapperFieldView : UIView

#pragma mark - Propertes

@property NSMutableArray *arrayCells;


#pragma mark - Methods

- (void)createField;

@end
