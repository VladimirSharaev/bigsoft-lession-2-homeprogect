//
//  SapperField.m
//  SapperProject
//
//  Created by Vladimir on 03.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "SapperField.h"

@implementation SapperField

#pragma mark - Methods

-(void)createField
{
    self.arrayOfCells = [[[NSMutableArray alloc] init] mutableCopy];
    for (NSInteger i = 0; i < FIELD_SIZE; i++) {
        SapperCell *cell = [SapperCell new];
        [self.arrayOfCells addObject:cell];
    }
    
    for (NSInteger i = 0 ; i < MINE_COUNT ; i++) {
        NSInteger randomNumber = arc4random() % 64;
        SapperCell *cell = self.arrayOfCells[randomNumber];
        cell.haveCellMine = YES;
        NSMutableArray *arrayOfIndex = [NSMutableArray new];
        arrayOfIndex = [self getIndexNearOpenCellSmallWersion:randomNumber];
        for (id object in arrayOfIndex) {
            if ([self correctIndex:randomNumber indexFromArray:[object integerValue]]) {
                cell = self.arrayOfCells[[object integerValue]];
                cell.numberOfMinesWhoNear++;

            }
        }
    }
}

- (BOOL)correctIndex:(NSInteger)index indexFromArray:(NSInteger)indexFromArray
{
    NSInteger row = index / FIELD_HEIGHT_COUNT;
    NSInteger collum = index % FIELD_WIDHT_COUNT;
    NSInteger rowIndexFromArray = indexFromArray / FIELD_HEIGHT_COUNT;
    NSInteger collumIndexFromArray = indexFromArray % FIELD_WIDHT_COUNT;
    
    if ((rowIndexFromArray >= row - 1) && (rowIndexFromArray <= row + 1) && (collumIndexFromArray >= collum - 1) && (collumIndexFromArray <= collum + 1) && (indexFromArray >= 0) && (indexFromArray <= FIELD_SIZE - 1)) {
        return YES;
    }
    return NO;
}

- (NSMutableArray *)getIndexNearOpenCellSmallWersion:(NSInteger)index
{
    NSMutableArray *arrayOfIndex = [NSMutableArray new];
    [arrayOfIndex addObject:@(index + 1)];
    [arrayOfIndex addObject:@(index - 1)];
    [arrayOfIndex addObject:@(index + FIELD_HEIGHT_COUNT)];
    [arrayOfIndex addObject:@(index - FIELD_HEIGHT_COUNT)];
    [arrayOfIndex addObject:@(index + (FIELD_HEIGHT_COUNT - 1))];
    [arrayOfIndex addObject:@(index + (FIELD_HEIGHT_COUNT + 1))];
    [arrayOfIndex addObject:@(index - (FIELD_HEIGHT_COUNT - 1))];
    [arrayOfIndex addObject:@(index - (FIELD_HEIGHT_COUNT + 1))];
    return arrayOfIndex;
}

- (NSInteger)getNumberOfMines:(NSInteger)index
{
    SapperCell *cell = self.arrayOfCells[index];
    return cell.numberOfMinesWhoNear;
}

- (BOOL)haveCellMine:(NSInteger)index
{
    SapperCell *cell = self.arrayOfCells[index];
    return cell.haveCellMine;
}



@end
