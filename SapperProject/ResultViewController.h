//
//  ResultViewController.h
//  SapperProject
//
//  Created by Vladimir on 14.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SapperPlayer.h"

@interface ResultViewController : UIViewController

#pragma mark - Propertes

@property (nonatomic) SapperPlayer *player;

#pragma mark - ViewPropertes

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;


@end
