//
//  Header.h
//  SapperProject
//
//  Created by Vladimir on 03.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#ifndef SapperProject_Header_h
#define SapperProject_Header_h

#define MINE_COUNT 8
#define FIELD_SIZE 64
#define FIELD_HEIGHT_COUNT 8
#define FIELD_WIDHT_COUNT 8
#define CELL_HEIGHT 33
#define CELL_WIDHT 33
#define FIELD_X 28
#define FIELD_Y 272
#define FIELD_VIEW_SIZE 264

#endif
